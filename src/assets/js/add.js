/*
Designed and Developed by Binu Senevirathne
2018/01/17
*/

const electron = require('electron');
const path = require('path');
const remote = electron.remote;
const ipc = electron.ipcRenderer;

const closeBtn = document.getElementById('close-btn');

closeBtn.addEventListener('click', function(event){
    var window = remote.getCurrentWindow();
    window.close();
});

const updateBtn = document.getElementById('update-btn');

updateBtn.addEventListener('click', function(event){
    ipc.send('update-notify-value', document.getElementById('notify-val').value);
    var window = remote.getCurrentWindow();
    window.close();
});